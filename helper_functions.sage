def ext(Fl,Fs,vec,is_my_extension):
    # Maps the vector vec over Fl to a matrix over Fs.
    # is_my_extension True means that Fl is a extension of an extension field, e.g., Fl = GF(2^2).extension(minimal_polynomial_beta)
    # is_my_extension False means that Fl is a 'regular' extensionfield, e.g., Fl = GF(2^6)
    u = Fl.degree()
    output = matrix(Fs,u,vec.ncols())
    if is_my_extension:
        for ii in range(vec.ncols()):
            output[:,ii] = matrix(Fs,vec[0,ii].list()).transpose()
    else:
        for ii in range(vec.ncols()):
            output[:,ii] = matrix(Fs,vec[0,ii]._vector_()).transpose()

    return output

def ext_inv(Fl,Fs,mat):
    # Maps the matrix mat over Fs to a vector over Fl
    m = Fl.degree()
    output = matrix(Fl,sum([mat[i,:]*(Fl.gen()^i) for i in range(m)]))
    return output

def trace(Fl,Fs,vec):
    # Trace operation from a vector vec over Fl to a projection over Fs
    val_mu = log(Fl.cardinality(),Fl.characteristic())
    val_m = log(Fs.cardinality(),Fs.characteristic())
    u = val_mu / val_m
    qm = Fs.cardinality()
    output = matrix(Fl,1,vec.ncols())
    for ii in range(vec.ncols()):
        output[0,ii] = sum([vec[0,ii]^(qm^j) for j in range(u)])
    return output


def rand_vec(Fl,Fs,rk,leng):
    # Generate a random vector of length leng over Fl with rank rk over Fs
    u = Fl.degree()
    # 1. Compute echelonizable matrix with rank rk
    rank_false = true
    while rank_false:
        A_mat = matrix.random(Fs,u,rk)
        if A_mat.rank()==rk:
            rank_false = false
    # print A_mat

    rank_false = true
    while rank_false:
        B_mat = matrix.random(Fs,rk,leng)
        if B_mat.rank()==rk:
            rank_false = false
    # print B_mat

    out_mat = A_mat*B_mat
    out_vec = ext_inv(Fl,Fs,out_mat)

    return out_vec

def dual_basis(Fq, basis):
    # Generate a dual basis
    basis = vector(basis[0,:])
    entries = [(basis[i] * basis[j]).trace() for i in range(Fq.degree()) for j in range(Fq.degree())]
    B = matrix(Fq.base_ring(), Fq.degree(), entries).inverse()
    db = [sum(map(lambda x: x[0] * x[1], zip(col, basis))) for col in B.columns()]
    return db

def Gab_ParityCheckMatrix(G):
    # Compute the Parity Check matrix for a generator matrix of a Gabidulin code G
    F = G.base_ring()
    q = F.base().cardinality()
    m = F.degree()
    n = G.ncols()
    k = G.nrows()
    g = G[0,:].list()

    Mhelp = matrix(F,n-1,n)
    for j in range(-n+k+1,k):
	Mhelp[j,:] = vector([g[i]^(q^(m+j)) for i in range(n)])

    hv = Mhelp[:,1:].solve_right(Mhelp[:,0])
    hv = [1] + hv.list()

    H = matrix([[hv[i]^(q^j) for i in range(n)] for j in range(n-k)])
    return H



def Gab_decoder(G,c,t):

    Fqm = G.base_ring()
    q   = Fqm.base().cardinality()
    m   = Fqm.degree()
    n_prime   = G.ncols()
    k   = G.nrows()

    H = Gab_ParityCheckMatrix(G)

    # 3.2.2 Compute the syndrome
    s = c * H.transpose()
    # This is just a sagemath thing: cast s to Fqm
    s = matrix(Fqm,[Fqm(s[0,ii].list()[0]) for ii in range(s.ncols())])

    if s == 0:
        return c

    # Set up the linear system of equations for the error span polynomial Lambda(x).
    S_t = matrix(Fqm,[[s[0,i+t-j]^(q^(j)) for j in range(t+1)] for i in range(n_prime-k-t)])

    # Solve the linear system of equations and derive the polynomial Lambda(x).
    Lam_v = S_t[:,1:].solve_right(S_t[:,0])
    Lambda = [1] + Lam_v.list()
    x = PolynomialRing(Fqm, 'x').gen() # define the variable name
    Lambda_x = sum([Lambda[i]*x^(q^i) for i in range(t+1)])

    # Find t linearly independent roots of Lambda(x) and the error value vector e_a.
    roots = Lambda_x.roots(multiplicities = False)
    roots_b = ext(Fqm,Fq,matrix(Fqm,roots),False)

    roots_echeform = roots_b.transpose().echelon_form().transpose()
    e_a = matrix(Fqm,ext_inv(Fqm,Fq,roots_echeform[:,:t]))

    #  Solve the linear system of equations to find the intermediate vector d_vec.
    d_A = matrix([[e_a[0,j]^(q^(m-i)) for j in range(t)] for i in range(n_prime-k)])
    d_b = matrix.column([s[0,i]^(q^(m-i)) for i in range(n_prime-k)])
    d_vec = d_A.solve_right(d_b).transpose()

    # Find e_B from d_vec s.t. e_B * h_0_b = d_vec_b.
    d_vec_b = ext(Fqm,Fq,matrix(Fqm,d_vec),False).transpose()

    h_0 = H[0, :]
    h_0_b = ext(Fqm,Fq,matrix(Fqm,h_0),False).transpose()
    e_B = h_0_b.solve_left(d_vec_b)

    # Calculate the error vector e and the decoded codeword c_dec.
    e_hat = e_a * e_B
    c_dec = c - e_hat

    return c_dec
